# Use the official Eclipse Temurin image as a parent image
# https://hub.docker.com/_/eclipse-temurin
FROM eclipse-temurin:11-jdk

# Argument to pass the JAR file at build time
# ARG JAR_FILE

# Set the user to root to perform installation and configuration
USER root

# Set environment variables for the HTTP service
ENV HTTP_SERVICE_HOME /usr/http-service
ENV HTTP_SERVICE_LOGS /var/log/http-service
ENV HTTP_SERVICE_USER http-svc
ENV HTTP_SERVICE_GROUP http-svc
ENV HTTP_SERVICE_ARTIFACT_NAME http-service.jar

# JAVA 11
# Set Java options for performance and security
ENV JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -XshowSettings:vm -XX:+UnlockExperimentalVMOptions -XX:MaxRAMFraction=1

# JAVA MEMORY
# Set Java memory options, can be overridden at runtime
ENV JAVA_MEMORY_OPTIONS ${JAVA_MEMORY_OPTIONS:--Xmx74M -XX:MaxMetaspaceSize=80M -Xss366K}

# Set environment variables for the locale
ENV TZ America/Mexico_City
ENV LANG es_MX.UTF-8
ENV LANGUAGE es_MX.UTF-8
ENV LC_ALL es_MX.UTF-8

# Install dependencies and configure locale and user
RUN apt-get update \
	&& apt-get install -y --no-install-recommends tzdata locales curl \
	# Enable the specified locale
    && sed -i "s/^# *\($LANG\)/\1/" /etc/locale.gen \
	# Generate the locale
	&& locale-gen && update-locale LANG=${LANG} LANGUAGE=${LANGUAGE} LC_ALL=${LC_ALL} \
	# Create a new group and user for the HTTP service
	&& addgroup --system --gid 2000 ${HTTP_SERVICE_GROUP} && adduser --system --uid 2000 ${HTTP_SERVICE_USER} --ingroup ${HTTP_SERVICE_GROUP} \
	# Create directories for the HTTP service and set permissions
	&& mkdir -p ${HTTP_SERVICE_HOME} \
	&& chown -R ${HTTP_SERVICE_USER} ${HTTP_SERVICE_HOME} \
	&& chgrp -R ${HTTP_SERVICE_GROUP} ${HTTP_SERVICE_HOME} \
	&& mkdir -p ${HTTP_SERVICE_LOGS} \
	&& chown -R ${HTTP_SERVICE_USER} ${HTTP_SERVICE_LOGS} \
	&& chgrp -R ${HTTP_SERVICE_GROUP} ${HTTP_SERVICE_LOGS}

# Define the volume for logs
VOLUME ${HTTP_SERVICE_LOGS}

# Expose port for the service
EXPOSE 8080

# Set the working directory inside the container
WORKDIR ${HTTP_SERVICE_HOME}

# Switch to the non-root user for security
USER ${HTTP_SERVICE_USER}

# Copy the JAR file to the container
# COPY ${JAR_FILE} ${HTTP_SERVICE_HOME}/${HTTP_SERVICE_ARTIFACT_NAME}

# Command to run the Java application
ENTRYPOINT java ${JAVA_OPTS} ${JAVA_MEMORY_OPTIONS} -jar ${HTTP_SERVICE_HOME}/${HTTP_SERVICE_ARTIFACT_NAME} ${SPRING_OPTS}